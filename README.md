# Poloniex Java Wrapper

This is an UNOFFICIAL java wrapper for https://poloniex.com. These scripts implement the reference specifications at https://poloniex.com/support/api/ as of Jan. 2nd, 2018.

- Use of this repository is at your own risk. I am not affiliated with Poloniex in any way.

- As of Jan. 2nd, 2018 this repository is not guaranteed to be maintained.