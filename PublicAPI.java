import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class PublicAPI {
	
	public static String getText(String url) throws MalformedURLException, IOException{
		Scanner p = new Scanner(new URL(url).openStream(), "UTF-8").useDelimiter("\\A");
		String out = p.next();
		p.close();
		return out;
	}
	
	public static String returnTicker(){
		try {
			return getText("https://poloniex.com/public?command=returnTicker");
		} catch (IOException e) {
			return null;
		}
	}
	
	public static String return24Volume(){
		try {
			return getText("https://poloniex.com/public?command=return24hVolume");
		} catch (IOException e) {
			return null;
		}
	}
	public static String returnOrderBook(){
		try {
			return getText("https://poloniex.com/public?command=returnOrderBook&currencyPair=all");
		} catch (IOException e) {
			return null;
		}
	}
	
	public static String returnOrderBook(String currencyPair){
		try {
			return getText("https://poloniex.com/public?command=returnOrderBook&currencyPair=" + currencyPair);
		} catch (IOException e) {
			return null;
		}
	}
	
	public static String returnOrderBook(String currencyPair, int depth){
		try {
			return getText("https://poloniex.com/public?command=returnOrderBook&currencyPair=" + currencyPair + "&depth=" + depth);
		} catch (IOException e) {
			return null;
		}
	}
	
	public static String returnTradeHistory(String currencyPair, long start, long end){
		try {
			return getText("https://poloniex.com/public?command=returnTradeHistory&currencyPair=" + currencyPair + "&start=" + start + "&end=" + end);
		} catch (IOException e) {
			return null;
		}
	}
	
	public static String returnChartData(String currencyPair, long start, long end, int period){
		try {
			return getText("https://poloniex.com/public?command=returnChartData&currencyPair=" + currencyPair + "&start=" + start + "&end=" + end + "&period=" + period);
		} catch (IOException e) {
			return null;
		}
	}
	public static String returnCurrencies(){
		try {
			return getText("https://poloniex.com/public?command=returnCurrencies");
		} catch (IOException e) {
			return null;
		}
	}
	public static String returnLoanOrders(String currency){
		try {
			return getText("https://poloniex.com/public?command=returnLoanOrders&currency=" + currency);
		} catch (IOException e) {
			return null;
		}
	}
}
