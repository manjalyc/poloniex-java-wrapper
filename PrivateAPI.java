import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import readers.StupidOrderedParser;

public class PrivateAPI {
	private String key;
	private String secret;
	
	public PrivateAPI(String key, String secret){
		this.key = key;
		this.secret = secret;
	}
	
	public String returnBalances(){
		return apiQuery("returnBalances");
	}
	
	public String returnCompleteBalances(){
		return apiQuery("returnCompleteBalances");
	}
	
	public String returnDepositAddresses(){
		return apiQuery("returnDepositAddresses");
	}
	
	public String generateNewAddress(String currency){
		String[] parameters = {"currency"};
		String[] values={currency};
		return apiQuery("generateNewAddress", parameters, values);
	}
	
	public String returnDepositsWithdrawals(long start, long end){
		String[] parameters = {"start", "end"};
		String[] values={Long.toString(start), Long.toString(end)};
		return apiQuery("generateNewAddress", parameters, values);
	}
	
	public String returnOpenOrders(){
		String[] parameters = {"currencyPair"};
		String[] values = {"all"};
		return apiQuery("returnOpenOrders", parameters, values);
	}
	
	public String returnOpenOrders(String currencyPair){
		String[] parameters = {"currencyPair"};
		String[] values = {currencyPair};
		return apiQuery("returnOpenOrders", parameters, values);
	}
	
	public String returnTradeHistory(){
		String[] parameters = {"currencyPair"};
		String[] values = {"all"};
		return apiQuery("returnTradeHistory", parameters, values);
	}
	
	public String returnTradeHistory(String currencyPair){
		String[] parameters = {"currencyPair"};
		String[] values = {currencyPair};
		return apiQuery("returnTradeHistory", parameters, values);
	}

	
	public String returnTradeHistory(long start, long end){
		String[] parameters = {"currencyPair", "start", "end"};
		String[] values = {"all", Long.toString(start), Long.toString(end)};
		return apiQuery("returnTradeHistory", parameters, values);
	}
	
	public String returnTradeHistory(String currencyPair, long start, long end){
		String[] parameters = {"currencyPair", "start", "end"};
		String[] values = {currencyPair, Long.toString(start), Long.toString(end)};
		return apiQuery("returnTradeHistory", parameters, values);
	}
	
	public String returnOrderTrades(long orderNumber){
		String[] parameters = {"orderNumber"};
		String[] values = {Long.toString(orderNumber)};
		return apiQuery("returnOrderTrades", parameters, values);
	}
	
	public String buy(String currencyPair, double rate, double amount){
		String[] parameters = {"currencyPair", "rate", "amount"};
		String[] values = {currencyPair, Double.toString(rate), Double.toString(amount)};
		return apiQuery("buy", parameters, values);
	}
	
	public String buy(String currencyPair, double rate, double amount, boolean fillOrKill, boolean immediateOrCancel, boolean postOnly){
		ArrayList<String> parameters = new ArrayList<>();
		ArrayList<String> values = new ArrayList<>();
		parameters.add("currencyPair");values.add(currencyPair);
		parameters.add("rate"); values.add(Double.toString(rate));
		parameters.add("amount"); values.add(Double.toString(amount));
		if(fillOrKill){parameters.add("fillOrKill");values.add("1");}
		if(immediateOrCancel){parameters.add("immediateOrCancel");values.add("1");}
		if(postOnly){parameters.add("postOnly");values.add("1");}
		return apiQuery("buy", parameters, values);
	}
	
	public String sell(String currencyPair, double rate, double amount){
		String[] parameters = {"currencyPair", "rate", "amount"};
		String[] values = {currencyPair, Double.toString(rate), Double.toString(amount)};
		return apiQuery("sell", parameters, values);
	}
	
	public String sell(String currencyPair, double rate, double amount, boolean fillOrKill, boolean immediateOrCancel, boolean postOnly){
		ArrayList<String> parameters = new ArrayList<>();
		ArrayList<String> values = new ArrayList<>();
		parameters.add("currencyPair");values.add(currencyPair);
		parameters.add("rate"); values.add(Double.toString(rate));
		parameters.add("amount"); values.add(Double.toString(amount));
		if(fillOrKill){parameters.add("fillOrKill");values.add("1");}
		if(immediateOrCancel){parameters.add("immediateOrCancel");values.add("1");}
		if(postOnly){parameters.add("postOnly");values.add("1");}
		return apiQuery("sell", parameters, values);
	}
	
	public String cancelOrder(long orderNumber){
		String[] parameters = {"orderNumber"};
		String[] values = {Long.toString(orderNumber)};
		return apiQuery("cancelOrder", parameters, values);
	}
	
	public String moveOrder(long orderNumber, double rate){
		String[] parameters = {"orderNumber", "rate"};
		String[] values = {Long.toString(orderNumber), Double.toString(rate)};
		return apiQuery("moveOrder", parameters, values);
	}
	
	public String withdraw(String currency, Double amount, String address){
		String[] parameters = {"currency", "amount", "address"};
		String[] values = {currency, Double.toString(amount), address};
		return apiQuery("withdraw",parameters, values);	
	}
	
	public String returnFeeInfo(){
		return apiQuery("returnFeeInfo");
	}
	
	public String returnAvailableAccountBalances(){
		return apiQuery("returnAvailableAccountBalances");
	}
	
	public String returnAvailableAccountBalances(String account){
		String[] parameters = {"account"};
		String[] values = {account};
		return apiQuery("returnAvailableAccountBalances",parameters,values);
	}
	
	public String returnTradableBalances(){
		return apiQuery("returnTradableBalances");
	}
	
	public String transferBalance(String currency, double amount, String fromAccount, String toAccount){
		String[] parameters = {"currency", "amount", "fromAccount", "toAccount"};
		String[] values = {currency, Double.toString(amount), fromAccount, toAccount};
		return apiQuery("transferBalance",parameters, values);
	}
	
	public String returnMarginAccountSummary(){
		return apiQuery("returnMarginAccountSummary");
	}
	
	public String marginBuy(String currencyPair, double rate, double amount){
		String[] parameters = {"currencyPair", "rate", "amount"};
		String[] values = {currencyPair, Double.toString(rate), Double.toString(amount)};
		return apiQuery("marginBuy", parameters, values);
	}
	
	public String marginBuy(String currencyPair, double rate, double amount, double lendingRate){
		String[] parameters = {"currencyPair", "rate", "amount", "lendingRate"};
		String[] values = {currencyPair, Double.toString(rate), Double.toString(amount), Double.toString(lendingRate)};
		return apiQuery("marginBuy", parameters, values);
	}
	
	public String marginSell(String currencyPair, double rate, double amount){
		String[] parameters = {"currencyPair", "rate", "amount"};
		String[] values = {currencyPair, Double.toString(rate), Double.toString(amount)};
		return apiQuery("marginSell", parameters, values);
	}
	
	public String marginSell(String currencyPair, double rate, double amount, double lendingRate){
		String[] parameters = {"currencyPair", "rate", "amount", "lendingRate"};
		String[] values = {currencyPair, Double.toString(rate), Double.toString(amount), Double.toString(lendingRate)};
		return apiQuery("marginSell", parameters, values);
	}
	
	public String getMarginPosition(){
		String[] parameters = {"currencyPair"};
		String[] values = {"all"};
		return apiQuery("getMarginPosition", parameters, values);
	}
	
	public String getMarginPosition(String currencyPair){
		String[] parameters = {"currencyPair"};
		String[] values = {currencyPair};
		return apiQuery("getMarginPosition", parameters, values);
	}
	
	public String closeMarginPosition(String currencyPair){
		String[] parameters = {"currencyPair"};
		String[] values = {currencyPair};
		return apiQuery("closeMarginPosition", parameters, values);
	}
	
	public String createLoanOffer(String currency, double amount, int duration, int autoRenew, double lendingRate){
		String[] parameters = {"currency", "amount", "duration", "autoRenew", "lendingRate"};
		String[] values = {currency, Double.toString(amount), Integer.toString(duration), Integer.toString(autoRenew), Double.toString(lendingRate)};
		return apiQuery("createLoanOffer", parameters, values);
	}
	
	public String cancelLoanOffer(long orderNumber){
		String[] parameters = {"orderNumber"};
		String[] values = {Long.toString(orderNumber)};
		return apiQuery("cancelLoanOffer", parameters, values);
	}
	
	public String returnOpenLoanOffers(){
		String[] parameters = {"currencyPair"};
		String[] values = {"all"};
		return apiQuery("returnOpenLoanOffers", parameters, values);
	}
	
	public String returnOpenLoanOffers(String currency){
		String[] parameters = {"currency"};
		String[] values = {currency};
		return apiQuery("returnOpenLoanOffers", parameters, values);
	}
	
	public String returnActiveLoans(){
		String[] parameters = {"currencyPair"};
		String[] values = {"all"};
		return apiQuery("returnActiveLoans", parameters, values);
	}
	
	public String returnActiveLoans(String currency){
		String[] parameters = {"currency"};
		String[] values = {currency};
		return apiQuery("returnActiveLoans", parameters, values);
	}
	
	public String returnLendingHistory(long start, long end){
		String[] parameters = {"start", "end"};
		String[] values = {Long.toString(start), Long.toString(end)};
		return apiQuery("returnLendingHistory", parameters, values);
	}
	
	public String returnLendingHistory(long start, long end, long limit){
		String[] parameters = {"start", "end", "limit"};
		String[] values = {Long.toString(start), Long.toString(end), Long.toString(limit)};
		return apiQuery("returnLendingHistory", parameters, values);
	}
	
	public String toggleAutoRenew(long orderNumber){
		String[] parameters = {"orderNumber"};
		String[] values = {Long.toString(orderNumber)};
		return apiQuery("toggleAutoRenew", parameters, values);
	}
	
	private String hmacSHA512(String value) {
        String result;
        try {
            Mac hmacSHA512 = Mac.getInstance("HmacSHA512");//initialize hmac Sha 512
            SecretKeySpec secretKeySpec = new SecretKeySpec(secret.getBytes(), "HmacSHA512");
            hmacSHA512.init(secretKeySpec);

            byte[] digest = hmacSHA512.doFinal(value.getBytes());
            BigInteger hash = new BigInteger(1, digest);
            result = hash.toString(16);//hexadecimal
            if ((result.length() % 2) != 0) {
                result = "0" + result;
            }
        } catch (IllegalStateException | InvalidKeyException | NoSuchAlgorithmException ex) {
            ex.printStackTrace();
            return null;
        }
        return result;
	}
	
	private long nonce(){
		return System.currentTimeMillis();
	}
	
	public String apiQuery(String command){
		return apiQuery(command, new String[]{}, new String[]{});
	}
	
	public String apiQuery(String command, String[] parameters, String[] values){
		try{
			String url = "https://poloniex.com/tradingApi";
			long nonce = nonce();
			String post_data = "nonce=" + nonce + "&command=" + command;
			
			for(int i = 0; i < parameters.length; i++){
				post_data += "&" + parameters[i] + "=" + values[i];
			}
			
			String sign = hmacSHA512(post_data);
			
			URL u = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) u.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Sign", sign);
			con.setRequestProperty("Key",key);
			
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(post_data);
			wr.flush();
			wr.close();
			
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			
			return response.toString();
		}
		catch(IOException e){
			return null;
		}
	}
	
	public String apiQuery(String command, ArrayList<String> parameters, ArrayList<String> values){
		try{
			String url = "https://poloniex.com/tradingApi";
			long nonce = nonce();
			String post_data = "nonce=" + nonce + "&command=" + command;
			
			for(int i = 0; i < parameters.size(); i++){
				post_data += "&" + parameters.get(i) + "=" + values.get(i);
			}
			
			//System.out.println(post_data);
			String sign = hmacSHA512(post_data);
			
			URL u = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) u.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Sign", sign);
			con.setRequestProperty("Key",key);
			
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(post_data);
			wr.flush();
			wr.close();
			
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			
			return response.toString();
		}
		catch(IOException e){
			return null;
		}
		
	}
	
}
